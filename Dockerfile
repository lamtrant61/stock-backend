FROM node:18

USER node

RUN mkdir -p /home/node/app && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY --chown=node:node . .

RUN npm install

ENV PORT=3000

EXPOSE ${PORT}

CMD npx sequelize-cli db:migrate; npx sequelize-cli db:seed:all; yarn start

# docker compose -p demo-project up -d
