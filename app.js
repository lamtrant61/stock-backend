var http = require("http");
var express = require("express");
var cookieParser = require("cookie-parser");
const cors = require("cors");

const { handleError } = require("./src/middlewares/handleError");
const { NotFoundError } = require("./src/cores/ApiError");

var socketIo = require("./src/sockets/config");
var login = require("./src/routes/login");
var logout = require("./src/routes/logout");
var register = require("./src/routes/register");
var changePass = require("./src/routes/changePass");
var getData = require("./src/routes/getData");
var getUser = require("./src/routes/getUser");
var handleUser = require("./src/routes/handleUser");
var stockDatabase = require("./src/routes/stockDatabase");

var testAuth = require("./src/routes/testAuth");

var app = express();
var server = http.createServer(app);
socketIo(server);

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/login", login);
app.use("/logout", logout);
app.use("/register", register);
app.use("/changePass", changePass);
app.use("/getData", getData);
app.use("/stockDatabase", stockDatabase);
app.use("/getUser", getUser);

// Get all users info use for admin only
app.use("/handleUser", handleUser);
app.use("/testAuth", testAuth);

app.all("*", (req, res, next) => {
  next(new NotFoundError("Page not found"));
});

app.use(handleError);

module.exports = server;
