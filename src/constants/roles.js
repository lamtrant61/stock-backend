const RoleCode = {
  ADMIN: "ADMIN",
  CUSTOMER: "CUSTOMER",
};

module.exports = {
  RoleCode,
};
