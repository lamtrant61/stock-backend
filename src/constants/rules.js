const { RoleCode } = require("./roles");

const getRule = (req) => {
  return {
    [RoleCode.ADMIN]: {
      ["User"]: {
        create: [],
        read: [],
        update: [],
        delete: [],
      },
    },
    [RoleCode.CUSTOMER]: {
      ["User"]: {
        read: [["username", "eq", req.username]],
      },
    },
  };
};

module.exports = {
  getRule,
};
