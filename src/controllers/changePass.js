const { findUserDatabase, updateUserDatabase } = require("../services/user");
const { SuccessMsgResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const asyncHandler = require("../utils/asyncHandler");
const { hash, compare } = require("../utils/bcrypt");
const { sequelize } = require("../database/models");

const changePassController = asyncHandler(async (req, res, next) => {
  const { oldPassword, newPassword, retypeNewPassword } = req.body;
  const t = await sequelize.transaction();
  req.transaction = t;
  const user = await findUserDatabase(req.username);
  if (!user) {
    await t.rollback();
    return next(new BadRequestError("User not found"));
  }
  const check = await compare(oldPassword, user.dataValues.password);
  if (!check) {
    await t.rollback();
    return next(new BadRequestError("Old password is incorrect"));
  }
  if (newPassword !== retypeNewPassword) {
    await t.rollback();
    return next(new BadRequestError("Retype password is not match"));
  }

  const hashedPassword = await hash(newPassword);
  await updateUserDatabase(user, { password: hashedPassword }, t);
  await t.commit();
  return new SuccessMsgResponse("Success").send(res);
});
module.exports = changePassController;
