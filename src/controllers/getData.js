const fs = require("fs");
const path = require("path");
const { SuccessResponse } = require("../cores/ApiResponse");
const asyncHandler = require("../utils/asyncHandler");

const getDataController = asyncHandler(async (req, res, next) => {
  const { code, state } = req.body;
  let rawdata = fs.readFileSync(
    "src/data/json/" + code + "_" + state + ".json"
  );
  rawdata = JSON.parse(rawdata);
  return new SuccessResponse("Success", rawdata).send(res);
});

const getImageController = asyncHandler(async (req, res, next) => {
  const { imageName } = req.body;
  const imagePath = path.join(__dirname, "../data/images/", imageName);
  res.setHeader("Content-Type", "image/png");
  return res.sendFile(imagePath);
});
module.exports = { getDataController, getImageController };
