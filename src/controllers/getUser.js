const asyncHandler = require("../utils/asyncHandler");
const { SuccessResponse } = require("../cores/ApiResponse");
const { findUserDatabase } = require("../services/user");
const { BadRequestError } = require("../cores/ApiError");

const getUserController = asyncHandler(async (req, res, next) => {
  const user = await findUserDatabase(req.username);
  if (!user) {
    return next(new BadRequestError("User not found"));
  }
  const { name, username, email } = user.dataValues;
  const role = user.dataValues.Role.dataValues.role;
  const result = {
    name,
    username,
    email,
    role,
  };

  return new SuccessResponse("Success", result).send(res);
});
module.exports = getUserController;
