const FactoryController = require("../factories/factory.controller");
const { User } = require("../database/models");

module.exports = new FactoryController(User);
