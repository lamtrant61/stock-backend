const asyncHandler = require("../utils/asyncHandler");
const { findUserDatabase } = require("../services/user");
const { updateTokenDatabase } = require("../services/token");
const { createToken } = require("../utils/handleToken");
const { SuccessResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const { compare } = require("../utils/bcrypt");
const { sequelize } = require("../database/models");

const login = asyncHandler(async (req, res, next) => {
  const { username, password } = req.body;
  const accessToken = createToken({
    tokenType: "access",
    username: username,
  });

  const t = await sequelize.transaction();

  req.transaction = t;
  const user = await findUserDatabase(username);
  if (!user) {
    await t.rollback();
    return next(new BadRequestError("User not found"));
  }
  const matchPassword = await compare(password, user.dataValues.password);
  if (!matchPassword) {
    await t.rollback();
    return next(new BadRequestError("Password is incorrect"));
  }
  const newRefreshToken = await updateTokenDatabase(user.dataValues.Token, t);

  await t.commit();
  return new SuccessResponse("Login success", {
    username: user.dataValues.username,
    accessToken,
    refreshToken: newRefreshToken.dataValues.token,
  }).send(res);
});
module.exports = {
  login,
};
