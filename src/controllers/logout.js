const asyncHandler = require("../utils/asyncHandler");
const { SuccessResponse } = require("../cores/ApiResponse");
const { findUserDatabase } = require("../services/user");
const { BadRequestError } = require("../cores/ApiError");
const { sequelize } = require("../database/models");
const { redisClient } = require("../configs/redis");
const { removeTokenDatabase } = require("../services/token");
require("dotenv").config();

const logout = asyncHandler(async (req, res, next) => {
  let accessToken = req.headers.authorization.split("Bearer ")[1];
  const user = await findUserDatabase(req.username);
  await redisClient.set(accessToken, "revoked", {
    EX: parseInt(process.env.REDIS_EXPIRE),
  });

  //let key = await redisClient.sendCommand(["KEYS", "*"]);
  //let data = await redisClient.sendCommand(["GET", key[0]]);
  await removeTokenDatabase(user.dataValues.Token);

  return new SuccessResponse("Success", {
    message: "Logout success",
  }).send(res);
});
module.exports = {
  logout,
};
