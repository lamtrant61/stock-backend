const asyncHandler = require("../utils/asyncHandler");
const { createUserDatabase, findUserDatabase } = require("../services/user");
const { createTokenDatabase } = require("../services/token");
const { createToken } = require("../utils/handleToken");
const { SuccessResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const { hash } = require("../utils/bcrypt");
const { sequelize } = require("../database/models");

const register = asyncHandler(async (req, res, next) => {
  const { name, username, password, email } = req.body;
  const accessToken = createToken({
    tokenType: "access",
    username: username,
  });

  const t = await sequelize.transaction();

  req.transaction = t;
  const userCheck = await findUserDatabase(username);
  if (userCheck) {
    await t.rollback();
    return next(new BadRequestError("User already exists"));
  }

  const tokenSave = await createTokenDatabase(t);
  const hashedPassword = await hash(password);
  const user = await createUserDatabase(
    {
      name,
      username,
      password: hashedPassword,
      email,
      tokenId: tokenSave.dataValues.id,
      roleId: 2,
    },
    t
  );
  await t.commit();
  return new SuccessResponse("Register success", {
    name: user.dataValues.name,
    username: user.dataValues.username,
    email: user.dataValues.email,
    accessToken,
    refreshToken: tokenSave.dataValues.token,
  }).send(res);
});
module.exports = {
  register,
};
