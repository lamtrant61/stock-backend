const { SuccessResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const asyncHandler = require("../utils/asyncHandler");
const { findStockData } = require("../services/stock");

const stockDatabaseController = asyncHandler(async (req, res, next) => {
  const { code } = req.body;
  let data = await findStockData(code);
  if (!data) return next(new BadRequestError("Cannot find stock data"));
  return new SuccessResponse("Success", data).send(res);
});
module.exports = { stockDatabaseController };
