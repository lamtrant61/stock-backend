const asyncHandler = require("../utils/asyncHandler");
const { SuccessResponse } = require("../cores/ApiResponse");
const { handleRule } = require("../utils/handleRule");
const { findUserCondition } = require("../services/user");

const demo = asyncHandler(async (req, res, next) => {
  const rule = handleRule([
    ["username", "eq", req.username],
    ["id", "eq", 2],
  ]);
  const data = await findUserCondition(rule);
  //console.log(data);
  if (req.newAccessToken) {
    return new SuccessResponse("Success", {
      test: "ok",
      newAccessToken: req.newAccessToken,
    }).send(res);
  }
  return new SuccessResponse("Success", {
    test: "not ok",
  }).send(res);
});
module.exports = {
  demo,
};
