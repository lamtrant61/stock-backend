"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("StockValues", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      stockId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Stocks",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
      },
      date: {
        type: Sequelize.STRING,
      },
      close: {
        type: Sequelize.FLOAT,
      },
      open: {
        type: Sequelize.FLOAT,
      },
      high: {
        type: Sequelize.FLOAT,
      },
      low: {
        type: Sequelize.FLOAT,
      },
      volume: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("StockValues");
  },
};
