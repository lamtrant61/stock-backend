const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class StockValue extends Model {
    static associate(models) {
      models.Stock.hasMany(StockValue, {
        foreignKey: "stockId",
      });
      StockValue.belongsTo(models.Stock, {
        foreignKey: "stockId",
      });
    }
  }
  StockValue.init(
    {
      stockId: DataTypes.INTEGER,
      date: DataTypes.STRING,
      close: DataTypes.FLOAT,
      open: DataTypes.FLOAT,
      high: DataTypes.FLOAT,
      low: DataTypes.FLOAT,
      volume: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "StockValue",
    }
  );
  return StockValue;
};
