const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Token extends Model {
    static associate(models) {
      Token.hasOne(models.User, {
        foreignKey: "tokenId",
      });
      models.User.belongsTo(Token, {
        foreignKey: "tokenId",
      });
    }
  }
  Token.init(
    {
      token: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Token",
    }
  );
  return Token;
};
