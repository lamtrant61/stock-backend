"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "Users",
      [
        {
          name: "admin",
          username: "admin",
          email: "dubaochungkhoan.24h@gmail.com",
          password:
            "$2b$10$7F9URpjLCzW97w6Bd6s3wugtL9BNV9B0RTr00eF0sVmeQmsVZIBUK", // 310898
          tokenId: 1,
          roleID: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Users", null, {});
  },
};
