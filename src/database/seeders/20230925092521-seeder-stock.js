"use strict";
const { getStockName } = require("../../sockets/getStockName");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    const codeData = getStockName("src/files/symbol_code.json");
    const dataInsert = codeData["symbol"].reduce((acc, cur, index) => {
      acc.push({
        name: cur,
        description: codeData["name"][index],
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      return acc;
    }, []);
    await queryInterface.bulkInsert("Stocks", dataInsert, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Stocks", null, {});
  },
};
