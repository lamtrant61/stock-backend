"use strict";
const { getStockName } = require("../../sockets/getStockName");
const Crawl_Stock = require("../../sockets/CrawlStock");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    let codeData = getStockName("src/files/symbol_code.json");
    const timeNow = new Date().getTime();
    for (let i = 0; i < codeData["symbol"].length; i++) {
      let code = codeData["symbol"][i];
      let crawlStock = new Crawl_Stock(code, 0, timeNow);
      let data = await crawlStock.getData();
      let dataInsert = data["data"].reduce((acc, cur, index) => {
        acc.push({
          stockId: i + 1,
          date: cur[0],
          close: cur[1],
          open: cur[2],
          high: cur[3],
          low: cur[4],
          volume: cur[5],
          createdAt: new Date(),
          updatedAt: new Date(),
        });
        return acc;
      }, []);
      await queryInterface.bulkInsert("StockValues", dataInsert, {});
    }
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("StockValues", null, {});
  },
};
