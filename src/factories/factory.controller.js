const asyncHandler = require("../utils/asyncHandler");
const { SuccessResponse, SuccessMsgResponse } = require("../cores/ApiResponse");
const { BadRequestError } = require("../cores/ApiError");
const { getRule } = require("../constants/rules");
const { handleRule } = require("../utils/handleRule");
const { sequelize } = require("../database/models");
const factoryService = require("./factory.service");

class FactoryController {
  constructor(Model) {
    this.Model = Model;
  }
  read() {
    return asyncHandler(async (req, res, next) => {
      const ruleRead = getRule(req)[req.role][this.Model.name]["read"];
      if (!ruleRead) return next(new BadRequestError("Permission denied"));
      let data = await factoryService.readModel(
        this.Model,
        handleRule(ruleRead)
      );
      if (!data)
        return next(new BadRequestError(`${this.Model.name} not found`));
      data = data.map((item) => item.dataValues);
      return new SuccessResponse("Success", data).send(res);
    });
  }
  update() {
    return asyncHandler(async (req, res, next) => {
      const ruleUpdate = getRule(req)[req.role][this.Model.name]["update"];
      if (!ruleUpdate) return next(new BadRequestError("Permission denied"));
      const dataUpdate = Object.keys(req.body).reduce((acc, item) => {
        if (
          item !== "id" &&
          item !== "withCredentials" &&
          item !== "refreshToken"
        )
          acc[item] = req.body[item];
        return acc;
      }, {});
      const t = await sequelize.transaction();
      req.transaction = t;
      let data = await factoryService.updateModel(
        this.Model,
        req.body.id,
        dataUpdate,
        handleRule(ruleUpdate),
        t
      );
      if (!data) {
        await t.rollback();
        return next(new BadRequestError(`${this.Model.name} not found`));
      }
      await t.commit();
      return new SuccessMsgResponse("Success").send(res);
    });
  }
  delete() {
    return asyncHandler(async (req, res, next) => {
      const ruleDelete = getRule(req)[req.role][this.Model.name]["delete"];
      if (!ruleDelete) return next(new BadRequestError("Permission denied"));
      const t = await sequelize.transaction();
      req.transaction = t;
      let data = await factoryService.deleteModel(
        this.Model,
        req.body.id,
        handleRule(ruleDelete),
        t
      );
      if (!data) {
        await t.rollback();
        return next(new BadRequestError(`${this.Model.name} not found`));
      }
      await t.commit();
      return new SuccessMsgResponse("Success").send(res);
    });
  }
}

module.exports = FactoryController;
