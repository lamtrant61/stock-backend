const { Op } = require("sequelize");

module.exports = {
  createModel: async (Model, data, t) => {
    return Model.create(data, { transaction: t });
  },
  readModel: async (Model, rule) => {
    return Model.findAll({
      where: rule,
    });
  },
  updateModel: async (Model, id, data, rule, t) => {
    const model = await Model.findOne({
      where: {
        [Op.and]: [{ id: id }, rule],
      },
    });
    if (!model) return null;
    for (const key in data) {
      model[key] = data[key];
    }
    await model.save({ transaction: t });
    return model;
  },
  deleteModel: async (Model, id, rule, t) => {
    const model = await Model.findOne({
      where: {
        [Op.and]: [{ id: id }, rule],
      },
    });
    if (!model) return null;
    await model.destroy({ transaction: t });
    return model;
  },
};
