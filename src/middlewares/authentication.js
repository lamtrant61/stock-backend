const { verifyToken, decodeAccessToken } = require("../utils/handleToken");
const { BadRequestError } = require("../cores/ApiError");
const { findUserDatabase } = require("../services/user");
const { createToken } = require("../utils/handleToken");
const { redisClient } = require("../configs/redis");
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = {
  handleBearerToken: async (req, res, next) => {
    let refreshToken;
    let tokenInfo;
    try {
      const bearerHeader = req.headers.authorization;
      refreshToken = req.body.refreshToken;
      if (typeof bearerHeader !== "undefined") {
        tokenInfo = bearerHeader.startsWith("Bearer ")
          ? bearerHeader.split("Bearer ")[1]
          : "";
        if (await redisClient.exists(tokenInfo)) {
          return next(new BadRequestError("Token has been revoked"));
        }
        const decoded = await verifyToken({
          token: tokenInfo,
          tokenType: "access",
        });
        const user = await findUserDatabase(decoded.username);
        if (!user) {
          return next(new BadRequestError("User not found"));
        }

        req.user = decoded;
        req.username = decoded.username;
        next();
      } else {
        return next(new BadRequestError("Invalid bearer token"));
      }
    } catch (error) {
      if (error instanceof jwt.TokenExpiredError) {
        try {
          const username = (await decodeAccessToken(tokenInfo)).username;
          const user = await findUserDatabase(username);
          if (refreshToken === user.dataValues.Token.dataValues.token) {
            await verifyToken({
              token: refreshToken,
              tokenType: "refresh",
            });
            const newAccessToken = createToken({
              username: username,
              tokenType: "access",
            });
            req.username = username;
            req.newAccessToken = newAccessToken;
            await redisClient.set(tokenInfo, "revoked", {
              EX: parseInt(process.env.REDIS_EXPIRE),
            });
            res.cookie("newAccessToken", newAccessToken, {
              expires: new Date(Date.now() + 10000),
            });
            return next();
          }
          return next(new BadRequestError("Token has expired"));
        } catch (error) {
          return next(new BadRequestError("Invalid token"));
        }
      }
      return next(new BadRequestError("Invalid token"));
    }
  },
};
