const { findUserDatabase } = require("../services/user");
const { BadRequestError } = require("../cores/ApiError");

module.exports = {
  handleAuthorization: async (req, res, next) => {
    try {
      const user = await findUserDatabase(req.username);
      if (!user) {
        return next(new BadRequestError("User not found"));
      }
      const role = user.dataValues.Role.dataValues.role;
      req.role = role.toUpperCase();
      next();
    } catch (error) {
      if (error instanceof BadRequestError) {
        return next(new BadRequestError("Invalid token"));
      } else {
        return next(new BadRequestError("Invalid authorization"));
      }
    }
  },
};
