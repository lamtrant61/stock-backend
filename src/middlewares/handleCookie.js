const { BadRequestError } = require("../cores/ApiError");

module.exports = {
  handleCookie: async (req, res, next) => {
    try {
      const { refreshToken } = req.cookies;
      req.body.refreshToken = refreshToken;
      req.params?.id ? (req.body.id = req.params.id) : "";
      req.params?.imageName ? (req.body.imageName = req.params.imageName) : "";
      next();
    } catch (error) {
      return next(new BadRequestError("Invalid refresh token"));
    }
  },
};
