const {
  NotFoundError,
  ApiError,
  InternalError,
  ErrorType,
} = require("../cores/ApiError");
require("dotenv").config();

function handleError(err, req, res, next) {
  if (err instanceof ApiError) {
    ApiError.handle(err, res);

    if (err.type === ErrorType.INTERNAL) {
      console.log(`500 - ${err.message} - ${req.originalUrl} - ${req.method}`);
    }
  } else {
    console.log(`500 - ${err.message} - ${req.originalUrl} - ${req.method}`);
    console.log(err);

    if (process.env.NODE_ENV === "development") {
      return res.status(500).send(err);
    }

    ApiError.handle(new InternalError(), res);
  }
}

module.exports = {
  handleError,
};
