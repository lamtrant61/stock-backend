const { BadRequestError } = require("../cores/ApiError");

ValidationSource = {
  BODY: "body",
  HEADER: "headers",
  QUERY: "query",
  PARAM: "params",
};

module.exports =
  (schema, source = ValidationSource.BODY) =>
  (req, _res, next) => {
    try {
      const { error } = schema.validate(req[source], {
        abortEarly: false,
      });

      if (!error) return next();

      const { details } = error;

      const message = details
        .map((i) => i.message.replace(/['"]+/g, ""))
        .join(", ");

      throw new BadRequestError(message);
    } catch (error) {
      next(error);
    }
  };
