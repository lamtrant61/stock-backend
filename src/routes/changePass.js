const express = require("express");
const router = express.Router();
const changePassController = require("../controllers/changePass");
const validator = require("../middlewares/validator");
const changePassSchema = require("../schemas/changePass");
const authentication = require("../middlewares/authentication");
const authorization = require("../middlewares/authorization");

router.use(authentication.handleBearerToken);
router.use(authorization.handleAuthorization);

router.route("/").post(validator(changePassSchema), changePassController);

module.exports = router;
