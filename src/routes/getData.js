const express = require("express");
const router = express.Router();
const {
  getDataController,
  getImageController,
} = require("../controllers/getData");
const validator = require("../middlewares/validator");
const getDataSchema = require("../schemas/getData");
const getImageSchema = require("../schemas/getImage");
const authentication = require("../middlewares/authentication");
const authorization = require("../middlewares/authorization");
const { handleCookie } = require("../middlewares/handleCookie");

router.use(authentication.handleBearerToken);
router.use(authorization.handleAuthorization);

router
  .route("/:imageName")
  .get(handleCookie, validator(getImageSchema), getImageController);
router.route("/").post(validator(getDataSchema), getDataController);

module.exports = router;
