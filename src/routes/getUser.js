const express = require("express");
const router = express.Router();
const getUserController = require("../controllers/getUser");
const validator = require("../middlewares/validator");
const tokenSchema = require("../schemas/token");
const authentication = require("../middlewares/authentication");
const authorization = require("../middlewares/authorization");

router.use(authentication.handleBearerToken);
router.use(authorization.handleAuthorization);

router.route("/").post(validator(tokenSchema), getUserController);

module.exports = router;
