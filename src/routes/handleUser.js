const express = require("express");
const router = express.Router();
const authentication = require("../middlewares/authentication");
const authorization = require("../middlewares/authorization");
const validator = require("../middlewares/validator");
const tokenSchema = require("../schemas/token");
const handleUser = require("../controllers/handleUser");
const updateUserSchema = require("../schemas/updateUser");
const deleteUserSchema = require("../schemas/deleteUser");
const { handleCookie } = require("../middlewares/handleCookie");

router.use(authentication.handleBearerToken);
router.use(authorization.handleAuthorization);
router.route("/").get(handleCookie, validator(tokenSchema), handleUser.read());
router
  .route("/:id")
  .put(handleCookie, validator(updateUserSchema), handleUser.update());
router
  .route("/:id")
  .delete(handleCookie, validator(deleteUserSchema), handleUser.delete());
module.exports = router;
