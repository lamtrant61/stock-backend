const express = require("express");
const router = express.Router();
const loginController = require("../controllers/login");
const validator = require("../middlewares/validator");
const loginSchema = require("../schemas/login");

router.route("/").post(validator(loginSchema), loginController.login);

module.exports = router;
