const express = require("express");
const router = express.Router();
const logoutController = require("../controllers/logout");
const validator = require("../middlewares/validator");
const tokenSchema = require("../schemas/token");
const authentication = require("../middlewares/authentication");

router.use(authentication.handleBearerToken);
router.route("/").post(validator(tokenSchema), logoutController.logout);

module.exports = router;
