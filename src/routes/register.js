const express = require("express");
const router = express.Router();
const registerController = require("../controllers/register");
const validator = require("../middlewares/validator");
const registerSchema = require("../schemas/register");

router.route("/").post(validator(registerSchema), registerController.register);

module.exports = router;
