const express = require("express");
const router = express.Router();
const { stockDatabaseController } = require("../controllers/stockDatabase");
const validator = require("../middlewares/validator");
const stockDatabaseSchema = require("../schemas/stockDatabase");
const authentication = require("../middlewares/authentication");

router.use(authentication.handleBearerToken);

router.route("/").post(validator(stockDatabaseSchema), stockDatabaseController);

module.exports = router;
