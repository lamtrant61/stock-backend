const express = require("express");
const router = express.Router();
const authentication = require("../middlewares/authentication");
const authorization = require("../middlewares/authorization");
const validator = require("../middlewares/validator");
const tokenSchema = require("../schemas/token");
const testAuth = require("../controllers/testAuth");

router.use(authentication.handleBearerToken);
router.use(authorization.handleAuthorization);
router.route("/").post(validator(tokenSchema), testAuth.demo);

module.exports = router;
