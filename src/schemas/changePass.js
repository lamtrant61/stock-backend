const Joi = require("joi");

const changePassSchema = Joi.object({
  refreshToken: Joi.string().min(50).max(200).required(),
  oldPassword: Joi.string().min(3).max(50).alphanum().required(),
  newPassword: Joi.string().min(3).max(50).alphanum().required(),
  retypeNewPassword: Joi.string()
    .min(3)
    .max(50)
    .alphanum()
    .valid(Joi.ref("newPassword"))
    .label("Retype password is not match")
    .required(),
});

module.exports = changePassSchema;
