const Joi = require("joi");

const deleteUserSchema = Joi.object({
  id: Joi.number().integer().min(1).required(),
  refreshToken: Joi.string().min(50).max(200).required(),
});

module.exports = deleteUserSchema;
