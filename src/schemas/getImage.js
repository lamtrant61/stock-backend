const Joi = require("joi");

const getImageSchema = Joi.object({
  refreshToken: Joi.string().min(50).max(200).required(),
  imageName: Joi.string().min(3).max(50).required(),
});

module.exports = getImageSchema;
