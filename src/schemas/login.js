const Joi = require("joi");

const loginSchema = Joi.object({
  username: Joi.string().min(3).max(50).alphanum().required(),
  password: Joi.string().min(3).max(50).alphanum().required(),
});

module.exports = loginSchema;
