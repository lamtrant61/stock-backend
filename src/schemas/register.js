const Joi = require("joi");

const registerSchema = Joi.object({
  name: Joi.string().min(3).max(50).alphanum().required(),
  username: Joi.string().min(3).max(50).alphanum().required(),
  password: Joi.string().min(3).max(50).alphanum().required(),
  email: Joi.string().email().min(10).max(100).required(),
});

module.exports = registerSchema;
