const Joi = require("joi");

const stockDatabaseSchema = Joi.object({
  refreshToken: Joi.string().min(50).max(200).required(),
  code: Joi.string().min(3).max(50).alphanum().required(),
});

module.exports = stockDatabaseSchema;
