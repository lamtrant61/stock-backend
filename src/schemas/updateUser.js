const Joi = require("joi");

const updateUserSchema = Joi.object({
  id: Joi.number().integer().min(1).required(),
  refreshToken: Joi.string().min(50).max(200).required(),
  name: Joi.string().min(3).max(50).alphanum().required(),
  username: Joi.string().min(3).max(50).alphanum().required(),
  email: Joi.string().email().min(10).max(100).required(),
  roleId: Joi.number().integer().max(2).min(1).required(),
  withCredentials: Joi.boolean().required(),
});

module.exports = updateUserSchema;
