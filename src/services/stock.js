const { Stock, StockValue } = require("../database/models");

module.exports = {
  findStockData: async (code) => {
    const stock = await Stock.findOne({
      where: { name: code },
      include: [
        {
          model: StockValue,
        },
      ],
    });
    return stock.StockValues.reduce((acc, cur) => {
      acc.push([
        parseInt(cur.dataValues.date),
        cur.dataValues.open,
        cur.dataValues.high,
        cur.dataValues.low,
        cur.dataValues.close,
        cur.dataValues.volume,
      ]);
      return acc;
    }, []);
  },
  findStockTimestamp: async (code, timestamp) => {
    return Stock.findOne({
      where: { name: code },
      include: [
        {
          model: StockValue,
          where: { date: timestamp },
        },
      ],
    });
  },
  createStockTimestamp: async (data, t) => {
    return StockValue.create(data, { transaction: t });
  },
  updateStockTimestamp: async (dataValue, data, t) => {
    return dataValue.update(data, { transaction: t });
  },
};
