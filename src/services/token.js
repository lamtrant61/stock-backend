const { Token } = require("../database/models");
const { createToken } = require("../utils/handleToken");

module.exports = {
  createTokenDatabase: async (t) => {
    const refreshToken = createToken({
      tokenType: "refresh",
    });
    return Token.create({ token: refreshToken }, { transaction: t });
  },
  updateTokenDatabase: async (token, t) => {
    const refreshToken = createToken({
      tokenType: "refresh",
    });
    return token.update({ token: refreshToken }, { transaction: t });
  },
  removeTokenDatabase: async (token) => {
    return token.update({ token: null });
  },
};
