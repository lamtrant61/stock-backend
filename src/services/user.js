const { User, Token, Role } = require("../database/models");

module.exports = {
  createUserDatabase: async (user, t) => {
    return User.create(user, { transaction: t });
  },
  findUserDatabase: async (username) => {
    return User.findOne({
      where: { username },
      include: [
        {
          model: Token,
        },
        {
          model: Role,
        },
      ],
    });
  },
  updateUserDatabase: async (user, data, t) => {
    for (const key in data) {
      user[key] = data[key];
    }
    return user.save();
  },
  findUserCondition: async (condition) => {
    return User.findAll({
      where: condition,
      include: [
        {
          model: Token,
        },
        {
          model: Role,
        },
      ],
    });
  },
};
