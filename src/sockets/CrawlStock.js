const axios = require("axios");

class Crawl_Stock {
  #url;
  constructor(symbol, startTime, endTime) {
    this.symbol = symbol;
    this.startTime = parseInt(startTime / 1000); // Time is milliseconds / 1000
    this.endTime = parseInt(endTime / 1000);
    this.#url = `https://dchart-api.vndirect.com.vn/dchart/history?resolution=D&symbol=${this.symbol}&from=${this.startTime}&to=${this.endTime}`;
  }
  getCurrentData = async () => {
    try {
      let dataCrawl = await axios.get(this.#url);
      if (dataCrawl.data.s === "ok") {
        let crawlTime = dataCrawl.data["t"];
        let crawlClose = dataCrawl.data["c"];
        let crawlOpen = dataCrawl.data["o"];
        let crawlHigh = dataCrawl.data["h"];
        let crawlLow = dataCrawl.data["l"];
        let crawVolume = dataCrawl.data["v"];
        if (crawlClose.length === 0) return null;
        else {
          return {
            date: crawlTime * 1000,
            close: crawlClose[crawlClose.length - 1],
            open: crawlOpen[crawlOpen.length - 1],
            high: crawlHigh[crawlHigh.length - 1],
            low: crawlLow[crawlLow.length - 1],
            volume: crawVolume[crawVolume.length - 1],
            symbol: this.symbol,
            status: "success",
          };
        }
      } else {
        return null;
      }
    } catch (err) {
      return null;
    }
  };
  getData = async () => {
    try {
      let dataCrawl = await axios.get(this.#url);
      if (dataCrawl.data.s === "ok") {
        let crawlTime = dataCrawl.data["t"];
        let crawlClose = dataCrawl.data["c"];
        let crawlOpen = dataCrawl.data["o"];
        let crawlHigh = dataCrawl.data["h"];
        let crawlLow = dataCrawl.data["l"];
        let crawVolume = dataCrawl.data["v"];
        let data = this.convertTypeData(
          crawlTime,
          crawlClose,
          crawlOpen,
          crawlHigh,
          crawlLow,
          crawVolume
        );
        return { data: data, symbol: this.symbol };
      } else {
        return null;
      }
    } catch (err) {
      return null;
    }
  };
  convertTypeData = (
    crawlTime,
    crawlClose,
    crawlOpen,
    crawlHigh,
    crawlLow,
    crawVolume
  ) => {
    let data = crawlTime.reduce((acc, cur, index) => {
      acc.push([
        cur * 1000,
        crawlClose[index],
        crawlOpen[index],
        crawlHigh[index],
        crawlLow[index],
        crawVolume[index],
      ]);
      return acc;
    }, []);
    return data;
  };
}

module.exports = Crawl_Stock;
