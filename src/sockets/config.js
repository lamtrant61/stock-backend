const socketIo = require("socket.io");
const { dataRealTime } = require("./dataRealTime");
require("./handleCrawlRedis");

const socket = (server) => {
  const io = socketIo(server);
  io.on("connection", (socket) => {
    const clientIp = socket.request.connection.remoteAddress;
    const clientId = socket.id;
    console.log(`${clientId} connected`);
    socket.on("disconnect", () => {
      console.log(`${clientId} disconnected`);
    });
  });
  dataRealTime(io);
};

module.exports = socket;
