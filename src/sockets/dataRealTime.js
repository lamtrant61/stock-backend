const { redisClient } = require("../configs/redis");
const { getStockName } = require("./getStockName");
require("dotenv").config();

let codeData = getStockName("src/files/symbol_code.json")["symbol"];

const handleData = async (io) => {
  for (let i = 0; i < codeData.length; i++) {
    let code = codeData[i];
    let data = await redisClient.get(code);
    data = JSON.parse(data);
    if (data) {
      io.emit(data.symbol, data);
    }
  }
};

const dataRealTime = (io) => {
  setInterval(() => {
    handleData(io);
  }, parseInt(process.env.SOCKET_INTERVAL));
};

module.exports = { dataRealTime };
