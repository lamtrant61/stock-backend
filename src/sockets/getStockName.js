const fs = require("fs");

let getStockName = (url) => {
  let urlData = fs.readFileSync(url);
  return JSON.parse(urlData);
};
module.exports = { getStockName };
