const Crawl_Stock = require("./CrawlStock");
const { redisClient } = require("../configs/redis");
const { getStockName } = require("./getStockName");
const {
  findStockTimestamp,
  createStockTimestamp,
  updateStockTimestamp,
} = require("../services/stock");
const { sequelize } = require("../database/models");
require("dotenv").config();

let codeData = getStockName("src/files/symbol_code.json")["symbol"];

(() => {
  setInterval(async () => {
    for (let i = 0; i < codeData.length; i++) {
      let code = codeData[i];

      // Get current date time in miliseconds
      const timeNow = new Date().getTime();

      // Get current date only in miliseconds
      let dateNow = new Date();
      dateNow.setHours(0, 0, 0, 0);
      dateNow = dateNow.getTime();

      const crawlStock = new Crawl_Stock(code, dateNow, timeNow);
      let data = await crawlStock.getCurrentData();
      //console.log(data);

      if (data) {
        await redisClient.set(code, JSON.stringify(data), {
          EX: parseInt(process.env.REDIS_EXPIRE_STOCK),
        });
        const t = await sequelize.transaction();
        try {
          let { date, close, open, high, low, volume } = data;

          let stock = await findStockTimestamp(code, date);
          if (stock) {
            await updateStockTimestamp(
              stock.dataValues.StockValues[0],
              {
                close,
                open,
                high,
                low,
                volume,
              },
              t
            );
          } else {
            await createStockTimestamp(
              { stockId: ++i, date, close, open, high, low, volume },
              t
            );
          }
          await t.commit();
        } catch (error) {
          console.log(error);
          await t.rollback();
        }
      }
    }
  }, process.env.CRAWL_INTERVAL);
})();
