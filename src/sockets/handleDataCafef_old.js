const cheerio = require("cheerio");
const axios = require("axios");
const { getStockName } = require("./getStockName");

let cafefData = getStockName("src/files/cafef_url.json");

function convertDate(time) {
  const time_split = time.split("/");
  const day = parseInt(time_split[0]);
  const month = parseInt(time_split[1]) - 1;
  const year = parseInt(time_split[2]);
  let date = new Date(year, month, day);
  return date.getTime() + 86400000;
}

async function crawlDataRealTime(hex_code) {
  try {
    let response = await axios.get(cafefData[hex_code], {
      timeout: 1000,
    });
    let html = response.data;
    let $ = cheerio.load(html);

    let dateTime = $(".dltl-wrap .dltlu-time")
      .text()
      .trim()
      .split(",")[1]
      .trim();
    dateTime = convertDate(dateTime);
    close_now = $(".dltlu-point").text().trim();
    vol_now = $("#CV").text().trim().replace(",", "").replace(/,/, "");

    try {
      let all_data_crawl = $(".dltl-price").text().split("\n");
      for (i = 0; i <= all_data_crawl.length; i++) {
        let data_i = all_data_crawl[i].trim();
        if (data_i == "Giá mở cửa") {
          break;
        }
      }
      open_now = all_data_crawl[i + 1].trim();
    } catch (err) {
      let all_data_crawl = $(".dtlu-price-detail").text().split("\n");
      for (i = 0; i <= all_data_crawl.length; i++) {
        let data_i = all_data_crawl[i].trim();
        if (data_i == "Giá mở cửa") {
          break;
        }
      }
      open_now = all_data_crawl[i + 1].trim();
    }

    try {
      let all_data_crawl = $(".dltl-price").text().split("\n");
      for (i = 0; i <= all_data_crawl.length; i++) {
        let data_i = all_data_crawl[i].trim();
        if (data_i == "Giá cao nhất") {
          break;
        }
      }
      ceil_now = all_data_crawl[i + 1].trim();
    } catch (err) {
      let all_data_crawl = $(".dtlu-price-detail").text().split("\n");
      for (i = 0; i <= all_data_crawl.length; i++) {
        let data_i = all_data_crawl[i].trim();
        if (data_i == "Giá cao nhất") {
          break;
        }
      }
      ceil_now = all_data_crawl[i + 1].trim();
    }

    try {
      let all_data_crawl = $(".dltl-price").text().split("\n");
      for (i = 0; i <= all_data_crawl.length; i++) {
        let data_i = all_data_crawl[i].trim();
        if (data_i == "Giá thấp nhất") {
          break;
        }
      }
      floor_now = all_data_crawl[i + 1].trim();
    } catch (err) {
      let all_data_crawl = $(".dtlu-price-detail").text().split("\n");
      for (i = 0; i <= all_data_crawl.length; i++) {
        let data_i = all_data_crawl[i].trim();
        if (data_i == "Giá thấp nhất") {
          break;
        }
      }
      floor_now = all_data_crawl[i + 1].trim();
    }
    return {
      date: dateTime,
      close: close_now,
      open: open_now,
      ceil: ceil_now,
      floor: floor_now,
      volume: vol_now,
      symbol: hex_code,
      status: "success",
    };
  } catch (error) {
    return null;
  }
}

module.exports = { crawlDataRealTime };
