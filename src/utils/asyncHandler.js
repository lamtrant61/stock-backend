const asyncHandler = (fn) => {
  return (req, res, next) => {
    Promise.resolve(fn(req, res, next)).catch(async (err) => {
      if (req.transaction) {
        await req.transaction.rollback();
      }
      next(err);
    });
  };
};

module.exports = asyncHandler;
