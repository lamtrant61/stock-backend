const bcrypt = require("bcrypt");
require("dotenv").config();

module.exports = {
  hash: async (password) => {
    return bcrypt.hash(password, parseInt(process.env.SALT_ROUND));
  },
  compare: async (password, hashedPassword) => {
    return bcrypt.compare(password, hashedPassword);
  },
};
