module.exports = {
  handleBearer: (bearerToken) => {
    let tokenInfo = bearerToken.startsWith("Bearer ")
      ? bearerToken.split("Bearer ")[1]
      : "";
    return tokenInfo;
  },
};
