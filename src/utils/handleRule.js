const { Op } = require("sequelize");

const getRule = (rule) => {
  let handleOp = null;
  switch (rule) {
    case "eq":
      handleOp = Op.eq;
      break;
    case "ne":
      handleOp = Op.ne;
      break;
    case "gt":
      handleOp = Op.gt;
      break;
    case "gte":
      handleOp = Op.gte;
      break;
    case "lt":
      handleOp = Op.lt;
      break;
    case "lte":
      handleOp = Op.lte;
      break;
    default:
      handleOp = Op.eq;
  }
  return handleOp;
};

const handleRule = (rules) => {
  let result = {
    [Op.and]: [],
  };
  rules.forEach((rule) => {
    let handleOp = getRule(rule[1]);
    result[Op.and].push({
      [rule[0]]: {
        [handleOp]: rule[2],
      },
    });
  });
  return result;
};

module.exports = {
  handleRule,
};
